# Basic skeltal

Basic skeleton for node-based website


## Dependencies

None

## Dev Dependencies

- [body-parser](https://ghub.io/body-parser): Node.js body parsing middleware
- [browser-sync](https://ghub.io/browser-sync): Live CSS Reload &amp; Browser Syncing
- [cookie-parser](https://ghub.io/cookie-parser): cookie parsing with signatures
- [debug](https://ghub.io/debug): small debugging utility
- [express](https://ghub.io/express): Fast, unopinionated, minimalist web framework
- [gulp](https://ghub.io/gulp): The streaming build system
- [gulp-autoprefixer](https://ghub.io/gulp-autoprefixer): Prefix CSS
- [gulp-clean-css](https://ghub.io/gulp-clean-css): Minify css with clean-css.
- [gulp-cli](https://ghub.io/gulp-cli): Command line interface for gulp
- [gulp-load-plugins](https://ghub.io/gulp-load-plugins): Automatically load any gulp plugins in your package.json
- [gulp-nodemon](https://ghub.io/gulp-nodemon): A gulp-friendly nodemon wrapper that restarts your app as you develop, and keeps your build system contained to one process.
- [gulp-sass](https://ghub.io/gulp-sass): Gulp plugin for sass
- [gulp-sass-import](https://ghub.io/gulp-sass-import): Define a default file to import from a directory path
- [gulp-uglify](https://ghub.io/gulp-uglify): Minify files with UglifyJS.
- [morgan](https://ghub.io/morgan): HTTP request logger middleware for node.js
- [pug](https://ghub.io/pug): A clean, whitespace-sensitive template language for writing HTML
- [serve-favicon](https://ghub.io/serve-favicon): favicon serving middleware with caching

## License

ISC
