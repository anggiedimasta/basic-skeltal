'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');
var nodemon = require('gulp-nodemon');
var $ = require('gulp-load-plugins')({
    DEBUG: false
});

gulp.task('sass', function () {
    return gulp.src("./dev/sass/app.sass")
        .pipe($.sass({
                outputStyle: 'expanded'
            })
            .on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest("./dev/stylesheets"))
        .pipe($.cleanCss())
        .pipe($.rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest("./public/stylesheets"));
});

gulp.task('uglify', function () {
    gulp.src('./dev/javascripts/**/*.js')
        .pipe($.uglify())
        .pipe($.rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./public/javascripts'));
});

gulp.task('browser-sync', ['nodemon'], function () {
    browserSync.init(null, {
        proxy: "http://localhost:5000",
        files: ["./public/**/*.*", "./dev/views/**/*.pug"],
        browser: "C:\\\\Program Files\\\\Firefox Developer Edition\\\\firefox.exe",
        port: 7000,
    });
});

gulp.task('nodemon', function (cb) {

    var started = false;

    return nodemon({
        script: 'app.js'
    }).on('start', function () {
        if (!started) {
            cb();
            started = true;
        }
    });
});

gulp.task('default', ['sass', 'uglify', 'browser-sync'], function () {
    gulp.watch(['./dev/sass/app.sass'], ['sass']);
    gulp.watch(['./dev/javascripts/**/*.js'], ['uglify']);
});